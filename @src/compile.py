import csv
import subprocess
import requests

with open('compile.csv','r') as fp:
    fichiers = csv.DictReader(fp, delimiter=';')
    for seance in fichiers:
        documents = seance['fichiers'].split(' ')
        for document in documents:
            subprocess.run(['latexmk','-pdf',document])
