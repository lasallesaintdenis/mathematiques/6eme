% vim: set ft=tex :

\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{fontawesome}
\usepackage{stmaryrd}
%\usepackage{cmbright}
%\usepackage{kpfonts}
\usepackage{lmodern}

%\pdfminorversion 7
\pdfobjcompresslevel 3

\PrerenderUnicode{é}

%\usepackage[colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

%\usepackage[a4paper,vmargin=12.7mm,hmargin=6.35mm,includefoot,includehead]{geometry}
%\usepackage[top=6.35mm,bottom=6.35mm,left=2cm,right=2cm,includefoot]{geometry}

%\PassOptionsToPackage{framemethod=tikz}{mdframed}
%\usepackage{tipfr}
\usepackage[tikz]{bclogo}
\usepackage{tikz}
\usetikzlibrary{circuits.ee.IEC}

\usepackage{tikz-cd}

\usepackage{tkz-euclide}
%\usepackage{tkz-tab}

\usetikzlibrary{hobby,shapes.misc}

\tikzset{cross/.style={cross out, draw, 
minimum size=2*(#1-\pgflinewidth), 
inner sep=0pt, outer sep=0pt}}

\usepackage{amsmath,amsfonts,amssymb}
\usepackage{mathrsfs}

\usepackage{mathtools}% To colour equation numbers in proof

\definecolor{prop}{RGB}{128,24,24}
\definecolor{preuve}{RGB}{24,24,128}

\usepackage{amsthm}
\newtheoremstyle{prop}{}{}{\color{prop}}{}{\color{prop}\bfseries}{}{ }{}
\theoremstyle{prop}
%\theoremstyle{definition}
%\newtheorem{definition}{Définition}
%\theoremstyle{plain}
\newtheoremstyle{prop}{}{}{\color{prop}}{}{\color{prop}\bfseries}{}{ }{}
\theoremstyle{prop}
\newtheorem{theoreme}{Théorème}
\newtheorem{proposition}{Proposition}
%\newtheorem{corollaire}[definition]{Corollaire}
%\newtheorem{lemme}[definition]{Lemme}
%\newtheorem{theoremeetdefinition}[definition]{Théorème et définition}
\theoremstyle{plain}
%\newtheorem*{exemple}{Exemple}
%\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\newenvironment{exemple}{\begin{example}}{\end{example}}

\let\oldproof\proof
\renewcommand{\proof}{\color{preuve}\oldproof}

%\usepackage{titling}
%\usepackage{titlesec}
%\usepackage{sectsty}

\usepackage{multicol}


\usepackage{eurosym}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{cutwin}
\usepackage{floatrow}

\usepackage[a]{esvect}
\usepackage[extdef]{delimset}

\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\usepackage{array,multirow,makecell}
\setcellgapes{1pt}
\makegapedcells
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}

\newenvironment{contreexemple}[1][]{%
  \begin{bclogo}[logo=\bcdz,noborder=true,barre=none]{#1}
    }{%
  \end{bclogo}
}

\newenvironment{savoirfaire}[1][]{%
\begin{bclogo}[logo=\bcoutil,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}
\newenvironment{savoirrediger}[1][]{%
\begin{bclogo}[logo=\bccrayon,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}
\newenvironment{info}[1][]{%
\begin{bclogo}[logo=\bcinfo,noborder=true,barre=none]{#1}
}{%
\end{bclogo}
}

\usepackage{multicol}
\setlength{\columnseprule}{0.8pt}

\usepackage[french]{algorithm2e}

\usepackage{datetime2}
\usepackage[mark]{gitinfo2}

\usepackage{babel}


\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[]
    \draw[white] (0,#1+0.8) -- (15,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (15,0.6) ;
  \end{tikzpicture}%
}

\everymath{\displaystyle{\everymath{}}}
